﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// The Scoreboard is a scoreboard. It does exactly what you think it does.
public class Scoreboard : MonoBehaviour {
	public static Scoreboard S; // The singleton for Scoreboard

	public GameObject prefabFloatingScore;

	[HideInInspector]
	public GameObject GameOverDisplay;
	[HideInInspector]
	public GameObject RoundResult;
	[HideInInspector]
	public GameObject BG;


	void Awake () {
		S = this;
	}

	void Start() {
		GameOverDisplay = gameObject.transform.Find ("GameOver").gameObject;
		RoundResult = gameObject.transform.Find ("RoundResult").gameObject;
		BG = gameObject.transform.Find ("BG").gameObject;
	}

	public void GameOver() {
		GameOverDisplay.SetActive (true);
		RoundResult.SetActive (true);
		BG.SetActive (true);
		if (Bartok.CURRENT_PLAYER.type == PlayerType.human) {
			GameOverDisplay.GetComponent<Text>().text = "You Win!";
			RoundResult.GetComponent<Text>().text = "Congratulations!";
		} else {
			GameOverDisplay.GetComponent<Text>().text = "Game Over";
			RoundResult.GetComponent<Text>().text = "Player " + Bartok.CURRENT_PLAYER.playerNum + " wins";
		}
	}
}
