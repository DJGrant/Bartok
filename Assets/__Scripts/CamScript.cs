﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CamScript : MonoBehaviour {

	Camera cam;
	float lastCamAspect;

	// Use this for initialization
	void Start () {
		cam = GetComponent<Camera>();
		lastCamAspect = cam.aspect;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if (cam.aspect != lastCamAspect) {
			if (cam.aspect < (4f / 3f)) {
				cam.orthographicSize = 14 / cam.aspect;
				Scoreboard.S.GetComponent<CanvasScaler> ().matchWidthOrHeight = 0f;
			} else {
				cam.orthographicSize = 10;
				Scoreboard.S.GetComponent<CanvasScaler> ().matchWidthOrHeight = 1f;
			}
		}
		lastCamAspect = cam.aspect;
	}
}
